# OpenML dataset: 3D_Estimation_using_RSSI_of_WLAN_dataset_complete_1_target

https://www.openml.org/d/45720

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

3D Location Estimation using RSSI of WLAN dataset.The 3D Location Estimation Using RSSI of Wireless LAN challengeaims to develop an AI/ML-based localization algorithm that canaccurately estimate the position of a receiver based on RSS informationfrom surrounding radio transmitters including height information(enabling the estimation of the target's 3D location).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45720) of an [OpenML dataset](https://www.openml.org/d/45720). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45720/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45720/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45720/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

